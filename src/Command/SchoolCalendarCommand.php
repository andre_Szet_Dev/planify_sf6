<?php

namespace App\Command;

use App\Service\SchoolCalendarService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:school-calendar',
    description: 'Add a short description for your command',
)]
class SchoolCalendarCommand extends Command
{
    public function __construct(
        private readonly SchoolCalendarService $schoolCalendarService
    ) {
        parent::__construct(
        );
    }

    protected function configure(): void
    {
        $this
            ->addArgument('year', InputArgument::OPTIONAL, 'Year')
            // ->addArgument('zone', InputArgument::REQUIRED, 'Zone')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $zone = strtoupper($_ENV['SCHOOL_CALENDAR_ZONE']);

        $yearArg = (int) $input->getArgument('year');
        $currentYear = $currentYear = (new \DateTime())->format('Y');

        if (0 === $yearArg || $yearArg < $currentYear) {
            $yearArg = $currentYear;
        }

        $this->schoolCalendarService->getSchoolCalendar($yearArg, $zone);

        $io->success('Periods updated.');

        return Command::SUCCESS;
    }
}
