<?php

namespace App\Repository;

use App\Entity\PlannedTask;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<PlannedTask>
 *
 * @method PlannedTask|null find($id, $lockMode = null, $lockVersion = null)
 * @method PlannedTask|null findOneBy(array $criteria, array $orderBy = null)
 * @method PlannedTask[]    findAll()
 * @method PlannedTask[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlannedTaskRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PlannedTask::class);
    }

    //    /**
    //     * @return PlannedTask[] Returns an array of PlannedTask objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('p')
    //            ->andWhere('p.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('p.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?PlannedTask
    //    {
    //        return $this->createQueryBuilder('p')
    //            ->andWhere('p.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
