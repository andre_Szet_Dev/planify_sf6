<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;

/**
 * @extends ServiceEntityRepository<User>
 *
 * @implements PasswordUpgraderInterface<User>
 *
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(PasswordAuthenticatedUserInterface $user, string $newHashedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', $user::class));
        }

        $user->setPassword($newHashedPassword);
        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush();
    }

    public function findImproved(array $order = null, ?string $limit = null, ?string $offset = null, array $filters = null, ?string $search = null): Query
    {
        $em = $this->getEntityManager();
        $select = 'u';
        $where = " WHERE u.roles LIKE '%ROLE_AGENT%' ";
        $ordre = ' ORDER BY u.createdAt ASC ';

        $ordre = ' ORDER BY u.'.$order[0].' '.$order[1];

        // /////// filtre par colonne \\\\\\\\\

        if (null != $filters) {
            foreach ($filters as $field => $value) {
                $where .= ' AND u.'.$field." LIKE '%$value%' ";
            }
        }

        // ///////// Recherche générale \\\\\\\\\
        if (null != $search) {
            $where .= " AND (u.id LIKE '%$search%' ";
            $where .= " OR u.firstName LIKE '%$search%' ";
            $where .= " OR u.lastName LIKE '%$search%' ";
            $where .= " OR u.email LIKE '%$search%') ";
        }

        $dql = 'SELECT '.$select." FROM App\Entity\User u ".$where.' '.$ordre;

        $query = $em->createQuery($dql);

        if (!is_null($offset) && -1 != $limit) {
            $query->setMaxResults($limit);
            $query->setFirstResult($offset);
        }

        return $query;
    }

    public function findAgents(?string $term, ?string $role)
    {
        $query = $this->createQueryBuilder('u')
                        ->where('u.isActive = true')
                        ->andWhere('u.roles LIKE :val')
                        ->setParameter('val', '%'.$role.'%')
                        ->orderBy('u.firstName', 'ASC');

        if ($term) {
            $query->andWhere('u.firstName LIKE :term')
                ->orWhere('u.lastName LIKE :term')
                ->setParameter('term', '%'.$term.'%');
        }

        return $query->getQuery()->getResult();
    }

    //    /**
    //     * @return User[] Returns an array of User objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('u')
    //            ->andWhere('u.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('u.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?User
    //    {
    //        return $this->createQueryBuilder('u')
    //            ->andWhere('u.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
