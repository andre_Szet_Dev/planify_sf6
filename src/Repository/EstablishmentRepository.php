<?php

namespace App\Repository;

use App\Entity\Establishment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Establishment>
 *
 * @method Establishment|null find($id, $lockMode = null, $lockVersion = null)
 * @method Establishment|null findOneBy(array $criteria, array $orderBy = null)
 * @method Establishment[]    findAll()
 * @method Establishment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EstablishmentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Establishment::class);
    }

    public function findImproved(array $order = null, ?string $limit = null, ?string $offset = null, array $filters = null, ?string $search = null): Query
    {
        $em = $this->getEntityManager();
        $select = 'e';
        $where = ' WHERE e.isActive = true ';
        $ordre = ' ORDER BY e.createdAt ASC ';

        $ordre = ' ORDER BY e.'.$order[0].' '.$order[1];

        // /////// filtre par colonne \\\\\\\\\

        if (null != $filters) {
            foreach ($filters as $field => $value) {
                $where .= ' AND e.'.$field." LIKE '%$value%' ";
            }
        }

        // ///////// Recherche générale \\\\\\\\\
        if (null != $search) {
            $where .= " AND (e.id LIKE '%$search%' ";
            $where .= " OR e.name LIKE '%$search%' ";
            $where .= " OR e.address1 LIKE '%$search%' ";
            $where .= " OR e.address2 LIKE '%$search%' ";
            $where .= " OR e.zipCode LIKE '%$search%' ";
            $where .= " OR e.city LIKE '%$search%') ";
        }

        $dql = 'SELECT '.$select." FROM App\Entity\Establishment e ".$where.' '.$ordre;

        $query = $em->createQuery($dql);

        if (!is_null($offset) && -1 != $limit) {
            $query->setMaxResults($limit);
            $query->setFirstResult($offset);
        }

        return $query;
    }

    //    /**
    //     * @return Establishment[] Returns an array of Establishment objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('e')
    //            ->andWhere('e.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('e.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?Establishment
    //    {
    //        return $this->createQueryBuilder('e')
    //            ->andWhere('e.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
