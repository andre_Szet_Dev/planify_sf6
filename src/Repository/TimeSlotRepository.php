<?php

namespace App\Repository;

use App\Entity\TimeSlot;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TimeSlot>
 *
 * @method TimeSlot|null find($id, $lockMode = null, $lockVersion = null)
 * @method TimeSlot|null findOneBy(array $criteria, array $orderBy = null)
 * @method TimeSlot[]    findAll()
 * @method TimeSlot[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TimeSlotRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TimeSlot::class);
    }

    /**
     * @return TimeSlot[] Returns an array of TimeSlot objects
     */
    public function findTimeSlotsByUserAndDate(User $user, \DateTimeInterface $startDate, \DateTimeInterface $endDate): array
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.user = :user')
            ->andWhere('t.startedAt >= :startDate')
            ->andWhere('t.endedAt <= :endDate')
            ->setParameters([
                'user' => $user->getId(),
                'startDate' => $startDate,
                'endDate' => $endDate,
            ])
            ->orderBy('t.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findTimeSlotsToDelete($planning, $start, $end): array
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.user = :user')
            ->andWhere('t.startedAt >= :startDate')
            ->andWhere('t.endedAt <= :endDate')
            ->andWhere('t.isValidated = false')
            ->setParameters([
                'user' => $planning->getAgent()->getId(),
                'startDate' => $start,
                'endDate' => $end,
            ])

            ->getQuery()
            ->getResult()
        ;
    }

    //    public function findOneBySomeField($value): ?TimeSlot
    //    {
    //        return $this->createQueryBuilder('t')
    //            ->andWhere('t.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
