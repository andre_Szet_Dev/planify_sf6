<?php

namespace App\Form;

use App\Entity\Planning;
use App\Entity\Type;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PlanningType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'attr' => [
                    'class' => 'form-control',
                ],
                'label_attr' => [
                    'class' => 'col-md-2 col-form-label',
                ],
            ])
            ->add('startedAt', DateType::class, [
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'flatpickr form-control',
                ],
                'label_attr' => [
                    'class' => 'col-md-2 col-form-label'],
            ])
            ->add('endedAt', DateType::class, [
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'flatpickr form-control',
                ],
                'label_attr' => [
                    'class' => 'col-md-2 col-form-label'],
            ])
            ->add('description', TextareaType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'form-control',
                ],
                'label_attr' => [
                    'class' => 'col-md-2 col-form-label'],
            ])
            ->add('type', EntityType::class, [
                'attr' => [
                    'class' => 'form-control select2',
                ],
                'label' => 'Time type <span>*</span>',
                'label_attr' => [
                    'class' => 'col-md-2 col-form-label form-label',
                ],
                'multiple' => false,
                'class' => Type::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('t')
                    ->where('t.isActive = 1')
                    ->andWhere('t.entity = \'Planning\'')
                    ->orderBy('t.name', 'DESC')
                    ;
                },
                'choice_label' => function (Type $type) {
                    return $type->getName();
                },
                'label_html' => true,
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Next step',
                'attr' => [
                    'class' => 'btn btn-success waves-effect waves-light col-4 w-auto',
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Planning::class,
        ]);
    }
}
