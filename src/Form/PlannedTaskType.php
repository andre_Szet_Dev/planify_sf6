<?php

namespace App\Form;

use App\Entity\Establishment;
use App\Entity\PlannedTask;
use App\Entity\Task;
use App\Entity\Type;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PlannedTaskType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $plannedTask = $options['data'];
        $builder
            ->add('day', HiddenType::class, [])
            ->add('startedAt', TimeType::class, [
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'flatpickr form-control',
                ],
            ])
            ->add('endedAt', TimeType::class, [
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'flatpickr form-control',
                ],
            ])
            ->add('description', TextType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('task', EntityType::class, [
                'attr' => [
                    'class' => 'form-control select2',
                ],
                'label' => 'Task  <span>*</span>',
                'label_attr' => [
                    'class' => 'form-label',
                ],
                'multiple' => false,
                'class' => Task::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('t')
                    ->where('t.isActive = 1')
                    ->orderBy('t.name', 'ASC')
                    ;
                },
                'choice_label' => function (Task $task) {
                    return $task->getName();
                },
                'placeholder' => '--- Choose a task ---',
                'label_html' => true,
            ])
            ->add('type', EntityType::class, [
                'attr' => [
                    'class' => 'form-control select2',
                ],
                'label' => 'Time type <span>*</span>',
                'label_attr' => [
                    'class' => 'form-label',
                ],
                'multiple' => false,
                'class' => Type::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('t')
                    ->where('t.isActive = 1')
                    ->andWhere('t.entity = \'PlannedTask\'')
                    ->orderBy('t.name', 'ASC')
                    ;
                },
                'choice_label' => function (Type $type) {
                    return $type->getName();
                },
                'label_html' => true,
            ])
            ->add('establishment', EntityType::class, [
                'attr' => [
                    'class' => 'form-control select2',
                ],
                'label' => 'Time type <span>*</span>',
                'label_attr' => [
                    'class' => 'form-label',
                ],
                'multiple' => false,
                'class' => Establishment::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('e')
                    ->where('e.isActive = 1')
                    ->orderBy('e.name', 'ASC')
                    ;
                },
                'choice_label' => function (Establishment $establishment) {
                    return $establishment->getName();
                },
                'label_html' => true,
            ]);

        if (is_null($plannedTask->getId())) {
            $builder
            ->add('submit', SubmitType::class, [
                'label' => 'Add',
                'attr' => [
                    'class' => 'btn btn-primary w-md',
                ],
            ]);
        } else {
            $builder
            ->add('edit', SubmitType::class, [
                'label' => 'Edit',
                'attr' => [
                    'class' => 'btn btn-primary w-md',
                ],
            ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => PlannedTask::class,
            'attr' => ['id' => 'form_planned_task'],
        ]);
    }
}
