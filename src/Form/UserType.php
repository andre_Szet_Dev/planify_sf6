<?php

namespace App\Form;

use App\Entity\Job;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $user = $options['data'];
        $builder
            ->add('email', EmailType::class, [
                'attr' => [
                    'class' => 'form-control',
                ],
                'label_attr' => [
                    'class' => 'col-md-2 col-form-label',
                ],
            ])
            // ->add('roles')
            // ->add('password')
            ->add('firstName', TextType::class, [
                'attr' => [
                    'class' => 'form-control',
                ],
                'label_attr' => [
                    'class' => 'col-md-2 col-form-label',
                ],
            ])
            ->add('lastName', TextType::class, [
                'attr' => [
                    'class' => 'form-control',
                ],
                'label_attr' => [
                    'class' => 'col-md-2 col-form-label',
                ],
            ])
            ->add('job', EntityType::class, [
                'attr' => [
                    'class' => 'form-control',
                ],
                'label_attr' => [
                    'class' => 'col-md-2 col-form-label',
                ],
                'class' => Job::class,
                'query_builder' => function (EntityRepository $er) {
                    $qb = $er->createQueryBuilder('j')
                        ->where('j.isActive = true')
                    ;

                    return $qb;
                },
                'choice_label' => function (Job $job) {
                    return $job->getName();
                },
            ])
            ->add('submit', SubmitType::class, [
                'label' => is_null($user->getId()) ? 'Create' : 'Edit',
                'attr' => [
                    'class' => 'btn btn-primary w-md',
                ],
            ])
            // ->add('createdAt')
            // ->add('updatedAt')
            // ->add('deletedAt')
            // ->add('isActive')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
