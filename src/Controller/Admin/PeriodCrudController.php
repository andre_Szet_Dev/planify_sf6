<?php

namespace App\Controller\Admin;

use App\Entity\Period;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class PeriodCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Period::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')
                ->hideOnForm(),
            TextField::new('name'),
            DateTimeField::new('startedAt')
                ->setFormat("'le' dd/MM/yyy 'à' HH:mm:ss"),
            DateTimeField::new('endedAt')
                ->setFormat("'le' dd/MM/yyy 'à' HH:mm:ss"),
            DateTimeField::new('createdAt')
                ->hideOnForm()
                ->setFormat("'le' dd/MM/yyy 'à' HH:mm:ss"),
            BooleanField::new('isActive')
                ->hideWhenCreating(),
        ];
    }
}
