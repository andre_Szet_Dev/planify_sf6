<?php

namespace App\Controller\Admin;

use App\Entity\Job;
use App\Repository\ServiceRepository;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class JobCrudController extends AbstractCrudController
{
    public function __construct(
        private EntityManagerInterface $em,
        private ServiceRepository $serviceRepository
    ) {
    }

    public static function getEntityFqcn(): string
    {
        return Job::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')
                ->hideOnForm(),
            TextField::new('name'),
            DateTimeField::new('createdAt')
                ->hideOnForm()
                ->setFormat("'le' dd/MM/yyy 'à' HH:mm:ss"),
            BooleanField::new('isActive')
                ->hideWhenCreating(),
            AssociationField::new('service'),
        ];
    }
}
