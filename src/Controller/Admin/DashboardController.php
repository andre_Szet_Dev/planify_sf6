<?php

namespace App\Controller\Admin;

use App\Entity\Establishment;
use App\Entity\Job;
use App\Entity\Period;
use App\Entity\Service;
use App\Entity\Status;
use App\Entity\Task;
use App\Entity\Type;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    #[Route('/{_locale}/admin', name: 'admin', locale: 'fr',
        format: 'html',
        requirements: [
            '_locale' => 'en|fr',
        ], )]
    public function index(): Response
    {
        // $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);
        // return $this->redirect($adminUrlGenerator->setController(UserCrudController::class)->generateUrl());
        $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);

        return $this->redirect($adminUrlGenerator->setController(UserCrudController::class)->generateUrl());
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Planify Administration')
            ->setLocales(['fr'])
        ;
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToCrud('User', 'fa fa-user', User::class);
        yield MenuItem::linkToCrud('Establishment', 'fa fa-building', Establishment::class);
        yield MenuItem::linkToCrud('Service', 'fa fa-briefcase', Service::class);
        yield MenuItem::linkToCrud('Period', 'fa fa-calendar', Period::class);
        yield MenuItem::linkToCrud('Status', 'fa fa-check', Status::class);
        yield MenuItem::linkToCrud('Type', 'fa fa-clock', Type::class);
        yield MenuItem::linkToCrud('Job', 'fa fa-users-gear', Job::class);
        yield MenuItem::linkToCrud('Task', 'fa fa-list-check', Task::class);
    }
}
