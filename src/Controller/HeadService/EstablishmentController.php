<?php

namespace App\Controller\HeadService;

use App\Entity\Establishment;
use App\Form\EstablishmentType;
use App\Repository\EstablishmentRepository;
use App\Service\DatatableQueryService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/head/service/establishment')]
class EstablishmentController extends AbstractController
{
    public function __construct(
        private EntityManagerInterface $em,
        private EstablishmentRepository $establishmentRepository,
        private DatatableQueryService $datatableQueryService
    ) {
    }

    #[Route('/', name: 'app_head_service_establishment')]
    public function index(): Response
    {
        return $this->render('/head_service/establishment/index.html.twig');
    }

    #[Route('/all/establishments/ajax', name: 'app_head_service_all_establishments')]
    public function getAllEstablishments(Request $request)
    {
        $data = $this->datatableQueryService->processDataTableRequest($request, $this->establishmentRepository);

        return $this->render('head_service/establishment/_ajax/all_establishment_ajax.html.twig', [
            'draw' => $request->query->getInt('draw', 1),
            'total_count' => count($data['pagination']),
            'filtered_count' => count($data['query']->getResult()),
            'pagination' => $data['pagination'],
        ]);
    }

    #[Route('/add', name: 'app_head_service_establishment_add')]
    public function add(Request $request): Response
    {
        $establishment = new Establishment();
        $form = $this->createForm(EstablishmentType::class, $establishment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($establishment);
            $this->em->flush();

            $this->addFlash('success', 'Etablissement ajouté avec succés.');

            return $this->redirectToRoute('app_head_service_establishment_add');
        }

        return $this->render('/head_service/establishment/form_upsert.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route('/edit/{establishment}', name: 'app_head_service_establishment_edit')]
    public function edit(Request $request, Establishment $establishment): Response
    {
        $form = $this->createForm(EstablishmentType::class, $establishment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($establishment);
            $this->em->flush();

            $this->addFlash('success', 'Etablissement modifié avec succés.');

            return $this->redirectToRoute('app_head_service_establishment_edit', ['establishment' => $establishment->getId()]);
        }

        return $this->render('/head_service/establishment/form_upsert.html.twig', [
            'form' => $form,
        ]);
    }
}
