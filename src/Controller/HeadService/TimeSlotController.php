<?php

namespace App\Controller\HeadService;

use App\Entity\User;
use App\Repository\PlanningRepository;
use App\Repository\TimeSlotRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Workflow\WorkflowInterface;

#[Route('head/service/time/slot')]
class TimeSlotController extends AbstractController
{
    private const ROLE = 'ROLE_AGENT';

    public function __construct(
        private UserRepository $userRepository,
        private PlanningRepository $planningRepository,
        private WorkflowInterface $planningRequestWorkflow,
        private TimeSlotRepository $timeSlotRepository
    ) {
    }

    #[Route('/', name: 'app_head_service_time_slot')]
    public function index(): Response
    {
        return $this->render('head_service/time_slot/index.html.twig', [
            'controller_name' => 'TimeSlotController',
        ]);
    }

    #[Route('/list/agents/ajax', name: 'app_head_service_time_slot_list_agents_ajax')]
    public function getListAgents(Request $request): Response
    {
        $term = $request->query->get('term');
        $agents = $this->userRepository->findAgents($term, $this::ROLE);

        return $this->render('head_service/time_slot/_ajax/list_agents_ajax.html.twig', [
            'data' => $agents,
        ]);
    }

    #[Route('/list/plannings/ajax/{user}', name: 'app_head_service_time_slot_list_plannings_ajax')]
    public function getListPlannings(Request $request): Response
    {
        $term = $request->query->get('term');
        $plannings = $this->planningRepository->findPlannings($term);

        $data = [];
        foreach ($plannings as $planning) {
            $url = $this->generateUrl('app_head_service_edit_planning', ['planning' => $planning->getId()]);

            if (!$this->planningRequestWorkflow->can($planning, 'to_finalize')) {
                $url = $this->generateUrl('app_head_service_planning_create_step_2', ['planning' => $planning->getId()]);
            }

            $data[] = [
                'planning' => $planning,
                'url' => $url,
            ];
        }

        return $this->render('head_service/time_slot/_ajax/list_plannings_ajax.html.twig', [
            'data' => $data,
        ]);
    }

    #[Route('/get/events/ajax/{user}', name: 'app_head_service_time_slot_get_events_ajax')]
    public function getEventByUser(Request $request, User $user)
    {
        $data = json_decode($request->getContent(), true);
        $startDate = new \DateTime($data['startedAt']);
        $endDate = new \DateTime($data['endedAt']);
        $data = $this->timeSlotRepository->findTimeSlotsByUserAndDate($user, $startDate, $endDate);

        return $this->render('head_service/time_slot/_ajax/get_events.html.twig', [
            'data' => $data,
        ]);
    }
}
