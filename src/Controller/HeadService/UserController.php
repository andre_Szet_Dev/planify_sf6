<?php

namespace App\Controller\HeadService;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use App\Service\DatatableQueryService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/head/service/user')]
class UserController extends AbstractController
{
    public function __construct(
        private EntityManagerInterface $em,
        private UserRepository $userRepository,
        private DatatableQueryService $datatableQueryService
    ) {
    }

    #[Route('/', name: 'app_head_service_user')]
    public function index(): Response
    {
        return $this->render('/head_service/user/index.html.twig', [
            'controller_name' => 'UserController',
        ]);
    }

    #[Route('/all/users/ajax', name: 'app_head_service_all_users')]
    public function getAllUsers(Request $request)
    {
        $data = $this->datatableQueryService->processDataTableRequest($request, $this->userRepository);

        return $this->render('head_service/user/_ajax/all_user_ajax.html.twig', [
            'draw' => $request->query->getInt('draw', 1),
            'total_count' => count($data['pagination']),
            'filtered_count' => count($data['query']->getResult()),
            'pagination' => $data['pagination'],
        ]);
    }

    #[Route('/add', name: 'app_head_service_user_add')]
    public function add(Request $request): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setRoles(['ROLE_USER', 'ROLE_AGENT']);
            $this->em->persist($user);
            $this->em->flush();

            $this->addFlash('success', 'Agent ajouté avec succés.');

            return $this->redirectToRoute('app_head_service_user_add');
        }

        return $this->render('/head_service/user/form_upsert.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route('/edit/{user}', name: 'app_head_service_user_edit')]
    public function edit(Request $request, User $user): Response
    {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($user);
            $this->em->flush();

            $this->addFlash('success', 'Agent modifié avec succés.');

            return $this->redirectToRoute('app_head_service_user_edit', ['user' => $user->getId()]);
        }

        return $this->render('/head_service/user/form_upsert.html.twig', [
            'form' => $form,
        ]);
    }
}
