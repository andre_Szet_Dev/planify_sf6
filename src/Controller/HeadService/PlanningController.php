<?php

namespace App\Controller\HeadService;

use App\Entity\PlannedTask;
use App\Entity\Planning;
use App\Entity\TimeSlot;
use App\Entity\User;
use App\Form\PlannedTaskType;
use App\Form\PlanningType;
use App\Repository\PeriodRepository;
use App\Repository\PlannedTaskRepository;
use App\Repository\TimeSlotRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Workflow\WorkflowInterface;

#[Route('/head/service/planning')]
class PlanningController extends AbstractController
{
    public function __construct(
        private EntityManagerInterface $em,
        private WorkflowInterface $planningRequestWorkflow,
        private PlannedTaskRepository $plannedTaskRepository,
        private PeriodRepository $periodRepository,
        private TimeSlotRepository $timeSlotRepository,
    ) {
    }

    #[Route('/create/step/1/{user}', name: 'app_head_service_planning_create_step_1')]
    public function createPlanningStep1(Request $request, User $user, EntityManagerInterface $em): Response
    {
        $planning = new Planning();
        $planning->setAgent($user);
        $form = $this->createForm(PlanningType::class, $planning);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $this->planningRequestWorkflow->apply($planning, 'to_name_ok');
                $em->persist($planning);
                $em->flush();

                return $this->redirectToRoute('app_head_service_planning_create_step_2', [
                    'planning' => $planning->getId(),
                ]);
            } catch (\Throwable $th) {
                $this->addFlash('error', 'Error during planning creation.');
            }
        }

        return $this->render('head_service/planning/create_planning_step_1.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/create/step/2/{planning}', name: 'app_head_service_planning_create_step_2')]
    public function createPlanningStep2(Request $request, Planning $planning): Response
    {
        $plannedTask = new PlannedTask();

        $plannedTask->setPlanning($planning);

        $form = $this->createForm(PlannedTaskType::class, $plannedTask);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($plannedTask);
            if ($this->planningRequestWorkflow->can($planning, 'to_planning_wip')) {
                $this->planningRequestWorkflow->apply($planning, 'to_planning_wip');
            }

            $this->em->flush();
        }

        return $this->render('head_service/planning/create_planning_step_2.html.twig', [
            'planning' => $planning,
            'form' => $form,
        ]);
    }

    #[Route('/finalize/{planning}/{start}/{end}', name: 'app_head_service_planning_finalize')]
    public function finalize(Request $request, Planning $planning, $start, $end)
    {
        $applyStart = $planning->getStartedAt();
        $applyEnd = $planning->getEndedAt();

        $message = 'Emploi du finalisé avec succès';
        // si l'emploi a déjà été finalisé alors on récupère tous les timeslots non validé et compris entre les dates de début et fin du semainier et on les supprime toutes
        if (isset($planning->getStatus()['modify'])) {
            $timeslotsToDelete = $this->timeSlotRepository->findTimeSlotsToDelete($planning, $start, $end);
            foreach ($timeslotsToDelete as $timeSlot) {
                $this->em->remove($timeSlot);
            }
            try {
                $this->planningRequestWorkflow->apply($planning, 'to_planning_wip');
            } catch (\Throwable $th) {
                dd($th);
            }
            $message = 'Emploi du temps modifié avec succès';
            // les dates de début et de fin sont celles renseigné dans le formulaire de modification au dessus du planning
            $applyStart = \DateTime::createFromFormat('Y-m-d', $start);
            $applyEnd = \DateTime::createFromFormat('Y-m-d', $end);
            $this->em->flush();
        }
        $plannedActivetasks = $this->plannedTaskRepository->findBy(['planning' => $planning->getId(), 'isActive' => true]);

        $currentDate = $applyStart;

        while ($currentDate <= $applyEnd) {
            $currentDayOfWeek = strtolower($currentDate->format('N'));
            if (!$this->periodRepository->isDateInHolidayPeriod($currentDate)) {
                foreach ($plannedActivetasks as $task) {
                    if ($currentDayOfWeek == $task->getDay()) {
                        $startedAt = $this->parseDateTime($currentDate, $task->getStartedAt());
                        $endedAt = $this->parseDateTime($currentDate, $task->getEndedAt());
                        // dd($startedAt);
                        $timeSlot = new TimeSlot();
                        $timeSlot->setUser($planning->getAgent());
                        $timeSlot->setEstablishment($task->getEstablishment());
                        $timeSlot->setTask($task->getTask());
                        $timeSlot->setStartedAt($startedAt);
                        $timeSlot->setEndedAt($endedAt);
                        $timeSlot->setType($task->getType());
                        $timeSlot->setPlannedTask($task);

                        // comme la tache est créé depuis un chef de service elle est automatiquement confirmée
                        $timeSlot->setIsConfirmed(true);

                        $this->em->persist($timeSlot);
                        $this->em->flush();
                    }
                }
            }
            $currentDate = \DateTime::createFromInterface($currentDate);
            $currentDate->modify('+1 day');
        }
        try {
            $this->planningRequestWorkflow->apply($planning, 'to_finalize');
            $this->em->persist($planning);
            $this->em->flush();
        } catch (\Throwable $th) {
            dd($th);
        }
        $this->addFlash('success', $message);

        return $this->render('head_service/time_slot/index.html.twig', [
            'user' => $planning->getAgent(),
        ]);
    }

    #[Route('/edit/planning/{planning}', name: 'app_head_service_edit_planning')]
    public function editPlanning(Request $request, Planning $planning): Response
    {
        if ($this->planningRequestWorkflow->can($planning, 'to_modify')) {
            $this->planningRequestWorkflow->apply($planning, 'to_modify');
        }

        $plannedTask = new PlannedTask();

        $plannedTask->setPlanning($planning);

        $form = $this->createForm(PlannedTaskType::class, $plannedTask);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($plannedTask);
            $this->em->flush();
        }

        $this->em->flush();

        return $this->render('head_service/planning/edit_planning.html.twig', [
            'planning' => $planning,
            'form' => $form,
        ]);
    }

    #[Route('/get/events/{planning}', name: 'app_head_service_planning_get_events')]
    public function getEventsAjax(Planning $planning): Response
    {
        $events = $this->plannedTaskRepository->findBy(['planning' => $planning->getId(), 'isActive' => true]);

        return $this->render('head_service/planning/_ajax/events_ajax.html.twig', [
            'events' => $events,
        ]);
    }

    // # !!!!!!!!!!!!!!Lors de l'ajout d'un event il y a deux requête ajax qui partent !!!!!!!!!!!!!!!!!!!!!!!!!!!
    #[Route('/planned/task/add/event/{planning}', name: 'app_head_service_planning_add_planned_task')]
    public function addTaskPlanned(Request $request, Planning $planning)
    {
        $plannedTask = new PlannedTask();

        $plannedTask->setPlanning($planning);

        $form = $this->createForm(PlannedTaskType::class, $plannedTask);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($plannedTask);
            $this->em->flush();
            $responseData =
                [
                    'id' => $plannedTask->getId(),
                    'title' => $plannedTask->getTask()->getName(),
                    'daysOfWeek' => $plannedTask->getDay(),
                    'startTime' => $plannedTask->getStartedAt()->format('H:i:s'),
                    'endTime' => $plannedTask->getEndedAt()->format('H:i:s'),
                    'extendedProps' => [
                            'eventDescription' => $plannedTask->getDescription(),
                            'eventPlace' => $plannedTask->getEstablishment()->getName(),
                        ],
                    'color' => $plannedTask->getTask()->getColor(),
                ];

            return new JsonResponse($responseData, Response::HTTP_OK);
        }

        return $this->render('head_service/planning/_ajax/planned_task_add_form.html.twig', [
            'form' => $form->createView()]);
    }

    #[Route('/planned/task/edit/event/{plannedTask}', name: 'app_head_service_edit_planned_task')]
    public function editTaskPlanned(Request $request, PlannedTask $plannedTask)
    {
        $form = $this->createForm(PlannedTaskType::class, $plannedTask);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($this->planningRequestWorkflow->can($plannedTask->getPlanning(), 'to_modify')) {
                $this->planningRequestWorkflow->apply($plannedTask->getPlanning(), 'to_modify');
            }
            $this->em->persist($plannedTask);
            $this->em->flush();

            return new Response('0', Response::HTTP_OK);
        }

        return $this->render('head_service/planning/_ajax/planned_task_edit_form.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/planned/task/edit/drop/event/{plannedTask}', name: 'app_head_service_edit_drop_planned_task')]
    public function editDropTaskPlanned(Request $request, PlannedTask $plannedTask)
    {
        $data = json_decode($request->getContent(), true);
        $plannedTask->setStartedAt(new \DateTime($data['startedAt']));
        $plannedTask->setEndedAt(new \DateTime($data['endedAt']));
        $plannedTask->setDay($data['day']);

        if ($this->planningRequestWorkflow->can($plannedTask->getPlanning(), 'to_modify')) {
            $this->planningRequestWorkflow->apply($plannedTask->getPlanning(), 'to_modify');
        }

        $this->em->persist($plannedTask);
        $this->em->flush();

        return new Response('0', Response::HTTP_OK);
    }

    #[Route('/planned/task/delete/event/{plannedTask}', name: 'app_head_service_delete_planned_task')]
    public function deleteTaskPlanned(Request $request, PlannedTask $plannedTask)
    {
        if ($this->planningRequestWorkflow->can($plannedTask->getPlanning(), 'to_modify')) {
            $this->planningRequestWorkflow->apply($plannedTask->getPlanning(), 'to_modify');
        }
        $plannedTask->setIsActive(false);
        $plannedTask->setDeletedAt(new \DateTime('now'));
        $this->em->persist($plannedTask);
        $this->em->flush();

        return new Response('0', Response::HTTP_OK);
    }

    private function parseDateTime(\DateTime $date, \DateTime $time)
    {
        $dateFormat = $date->format('Y-m-d');

        // Extraire les parties de l'heure
        $timeFormat = $time->format('H:i:s');

        // Fusionner la date et l'heure dans une seule chaîne
        $combinedDateTimeString = $dateFormat.' '.$timeFormat;

        // Créer un nouvel objet DateTime à partir de la chaîne combinée
        return new \DateTime($combinedDateTimeString);
    }
}
