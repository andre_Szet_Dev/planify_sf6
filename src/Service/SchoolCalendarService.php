<?php

namespace App\Service;

use App\Entity\Period;
use App\Repository\PeriodRepository;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;

class SchoolCalendarService
{
    private const INCREMENT_YEAR = 3;

    public function __construct(
        private EntityManagerInterface $em,
        private PeriodRepository $periodRepository
    ) {
    }

    public function getSchoolCalendar(int $year, string $zone)
    {
        $client = new Client();
        $maxYear = $year + $this::INCREMENT_YEAR;

        while ($year <= $maxYear) {
            $period = [];

            $nextYear = $year + 1;
            $apiUrl = 'https://data.education.gouv.fr/api/explore/v2.1/catalog/datasets/fr-en-calendrier-scolaire/records?select=description%2C%20start_date%2C%20end_date%2C%20zones&where=annee_scolaire%20%3D%20%22'.$year.'-'.$nextYear.'%22&limit=100&refine=zones%3A%22Zone%20'.$zone.'%22';

            $response = $client->get($apiUrl);

            $data = $response->getBody()->getContents();

            $data = (array) json_decode($data);

            // vacances
            foreach ($data['results'] as $key => $value) {
                $name = $value->description.' '.substr($value->start_date, 0, 4);
                $exist = $this->periodRepository->findBy(['name' => $name]);
                if (!isset($period[$name]) && !$exist) {
                    $period[$name] = [
                        'start_date' => new \DateTime($value->start_date),
                        'end_date' => new \DateTime($value->end_date),
                    ];
                }
            }

            // jours férié

            $apiUrl = 'https://calendrier.api.gouv.fr/jours-feries/metropole/'.$year.'.json';

            $response = $client->get($apiUrl);

            $data = $response->getBody()->getContents();

            $data = (array) json_decode($data);

            $apiUrl = 'https://calendrier.api.gouv.fr/jours-feries/metropole/'.$nextYear.'.json';

            $response = $client->get($apiUrl);

            $dataNextYear = $response->getBody()->getContents();

            $dataNextYear = (array) json_decode($dataNextYear);

            $data = array_merge($data, $dataNextYear);

            foreach ($data as $key => $value) {
                $date = new \DateTime($key);
                $existedPeriod = $this->periodRepository->findBy(['startedAt' => $date]);

                if (!$existedPeriod) {
                    $period[$value.' '.substr($key, 0, 4)] = [
                        'start_date' => $date,
                        'end_date' => $date,
                    ];
                }
            }

            // persist in db
            foreach ($period as $key => $value) {
                $newPeriod = new Period();
                $newPeriod->setName($key);
                $newPeriod->setStartedAt($value['start_date']);
                $newPeriod->setEndedAt($value['end_date']);

                $this->em->persist($newPeriod);
                $this->em->flush();
            }

            ++$year;
        }
    }
}
