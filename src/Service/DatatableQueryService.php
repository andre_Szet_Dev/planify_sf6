<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;

class DatatableQueryService
{
    public function __construct(
        private EntityManagerInterface $em,
        private PaginatorInterface $paginator
    ) {
    }

    public function processDataTableRequest(Request $request, $repository)
    {
        $parameters = $request->query->all();

        $columns = $parameters['columns'];
        $array_orders = ['name', 'asc'];
        $orders = $parameters['order'];
        foreach ($orders as $order) {
            if (array_key_exists('column', $order)) {
                $col_pos = $order['column'];
                $column_name = $columns[$col_pos]['name'];
                $array_orders[0] = $column_name;
                $array_orders[1] = strtoupper($order['dir']);
            }
        }

        $filters = [];
        foreach ($columns as $col_pos => $column) {
            if (array_key_exists('search', $column)) {
                $column_name = $columns[$col_pos]['name'];
                $search_value = $columns[$col_pos]['search']['value'];

                if ('' != $search_value) {
                    $filters[$column_name] = $search_value;
                }
            }
        }

        $query = $repository->findImproved(
            $array_orders,
            $parameters['length'],
            $parameters['start'],
            $filters,
            $parameters['search']['value']
        );

        $pagination = $this->paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /* page number */
            $parameters['length']/* limit per page */
        );

        return ['pagination' => $pagination, 'query' => $query];
    }
}
