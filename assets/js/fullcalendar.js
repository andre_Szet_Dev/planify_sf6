import routes from "./routes";
import { Calendar } from '@fullcalendar/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import listPlugin from '@fullcalendar/list';
import interactionPlugin from '@fullcalendar/interaction';
import * as bootstrap from 'bootstrap';

// intervalle de temps par défaut dans le formulaire de création d'événement
const DEFAULT_SLOT = 2;

let calendar;  // Déclarer la variable à l'extérieur de la fonction

window.initFullCalendar = function (userId = null) {
    const calendarEl = document.getElementById('calendar');

    if (calendarEl) {
        calendar = new Calendar(calendarEl, {
            height: 650,
            locale: 'frLocale',
            plugins: [dayGridPlugin, timeGridPlugin, listPlugin],
            initialView: 'dayGridMonth',
            headerToolbar: {
                left: 'prev,next today',
                center: 'title',
                right: 'dayGridMonth,timeGridWeek,listWeek'
            },
            slotMinTime: '06:00:00',
            slotMaxTime: '20:00:00',
            allDaySlot: false,
            hiddenDays: [0, 6],
            datesSet: (info) => {
                let start = info.startStr;
                let end = info.endStr;

                if (userId != null) {

                    fetch(routes.app_head_service_time_slot_get_events_ajax + userId, {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                            startedAt: start,
                            endedAt: end
                        }),
                    })
                        .then(response => {
                            if (!response.ok) {
                                throw new Error('Erreur lors du chargement des évennements.');
                            }
                            return response.json();
                        })
                        .then(data => {
                            calendar.removeAllEvents();
                            calendar.addEventSource(data);
                        })
                        .catch(error => {
                            console.error('Erreur :', error);
                        })
                }
            }
        });

        // if(user != null){
        //     calendar.events = {
        //         url: routes.app_head_service_planning_get_events + planningId,
        //         method: 'GET'
        //     }
        // }
        calendar.render();
    }
}

window.initCalendarWeekly = function () {
    var weeklyEl = document.getElementById('calendar-weekly');

    if (weeklyEl) {
        const modalEventFC = document.getElementById('modalEventFC');
        const modalBS = new bootstrap.Modal(modalEventFC);

        
        const formCreateEvent = document.getElementById('form_planned_task');
        const btnSubmitCreateForm = document.getElementById('planned_task_submit');
        const planningId = weeklyEl.dataset.planningId;

        calendar = new Calendar(weeklyEl, {
            height: 650,
            plugins: [timeGridPlugin, interactionPlugin],
            locale: 'frLocale',
            initialView: 'timeGridWeek',
            initialDate: '1970-12-01',
            slotDuration: '00:15:00',
            slotMinTime: '06:00:00',
            slotMaxTime: '20:00:00',
            allDaySlot: false,
            hiddenDays: [0, 6],
            dayHeaderFormat: {
                weekday: 'long'
            },
            editable: true,
            headerToolbar: false,
            dateClick: function (info) {
                loadEventAddForm(this, info, planningId)
            },
            events: {
                url: routes.app_head_service_planning_get_events + planningId,
                method: 'GET'
            },
            eventContent: function (arg) {
                var eventDescription = arg.event.extendedProps.eventDescription;
                var eventPlace = arg.event.extendedProps.eventPlace;

                var html = '<div class="event-content">'
                    + '<b>Horaire: </b>' + arg.timeText + '<br>'
                    + '<b>Tâche: </b>: ' + arg.event.title + '<br>'
                    + '<b>Lieu: </b>' + eventPlace + '<br>';

                if (eventDescription != "")
                    html += '<b>Note: </b>' + eventDescription;

                html += '</div>';
                return {
                    html: html
                };
            },

            eventClick: function (info) {
                
                loadEventEditForm(info.event.id, this);
            },
            eventDrop: function (info) {
                var eventId = info.event.id;
                var newStartDate = parseTime(info.event.start);
                var newEndDate = parseTime(info.event.end);
                var day = info.event.start.getDay();

                fetch(routes.app_head_service_edit_drop_planned_task + eventId, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        startedAt: newStartDate,
                        endedAt: newEndDate,
                        day: day,
                    }),
                })
                    .then(response => {
                        if (!response.ok) {
                            throw new Error('Erreur lors du déplacement de l\'événement');
                        }
                        return response.json();
                    })
                    .then(data => {
                        console.log(data);
                    })
                    .catch(error => {
                        console.error('Erreur :', error);
                    });
            },
            eventResize: function (info) {
                var eventId = info.event.id;
                var newStartDate = parseTime(info.event.start);
                var newEndDate = parseTime(info.event.endcol - 1);
                var day = info.event.start.getDay();

                if (info.event.start.getDay() == info.event.end.getDay()) {
                    fetch(routes.app_head_service_edit_drop_planned_task + eventId,
                        {
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/json',
                            },
                            body: JSON.stringify({
                                startedAt: newStartDate,
                                endedAt: newEndDate,
                                day: day,
                            }),
                        })
                        .then(response => {
                            if (!response.ok) {
                                throw new Error('Erreur lors du déplacement de l\'événement');
                            }
                            return response.json();
                        })
                        .then(data => {
                            console.log(data);
                        })
                        .catch(error => {
                            console.error('Erreur :', error);
                        });
                } else {
                    info.revert();
                }
            }
        });

        calendar.render();
    }
}

function parseTime(time) {
    return new Intl.DateTimeFormat('fr-FR', {
        hour: 'numeric',
        minute: 'numeric',
        hour12: false
    }).format(time);
}

// function addEvent(){
//     fetch('/head/service/planning/create/step/2/4');
// }

function loadEventAddForm(calendar, info, planningId) {
    var formRoute = routes.app_head_service_planning_add_planned_task + planningId;
    fetch(formRoute)
        .then(response => {
            if (!response.ok) {
                throw new Error('Erreur lors de la récupération du formulaire');
            }
            return response.text();
        })
        .then(data => {
            let modalEvent = document.getElementById('modalEventFC');

            modalEvent.innerHTML = data;
            modalEvent = new bootstrap.Modal(modalEvent);
            modalEvent.show();

            const start_input = document.getElementById('planned_task_startedAt');
            const end_input = document.getElementById('planned_task_endedAt');
            const day_input = document.getElementById('planned_task_day');

            var clickedTime = info.date;
            var slotTime = new Date(clickedTime.getTime() + DEFAULT_SLOT * 60 * 60 * 1000);
            var formattedClickedTime = parseTime(clickedTime);
            var formattedSlotTime = parseTime(slotTime);


            start_input.value = formattedClickedTime;
            end_input.value = formattedSlotTime;
            day_input.value = clickedTime.getDay();

            const form = document.getElementById('form_planned_task');
            const submit_btn = document.getElementById('planned_task_submit');
            submit_btn.addEventListener('click', function (event) {
                event.preventDefault();
                handleFormSubmission(calendar,form, null, planningId);
                modalEvent.hide();
            });
        })
        .catch(error => {
            console.error('Erreur :', error);
        });
}

function loadEventEditForm(eventId, calendar) {
    var formRoute = routes.app_head_service_edit_planned_task + eventId;
    fetch(formRoute)
        .then(response => {
            if (!response.ok) {
                throw new Error('Erreur lors de la récupération du formulaire');
            }
            return response.text();
        })
        .then(data => {
            let modalEvent = document.getElementById('modalEditFC');

            modalEvent.innerHTML = data;
            modalEvent = new bootstrap.Modal(modalEvent);
            modalEvent.show();

            const form = document.getElementById('form_planned_task');
            const submit_btn = document.getElementById('planned_task_edit');
            const delete_btn = document.getElementById('planned_task_delete');
            submit_btn.addEventListener('click', function (event) {
                event.preventDefault();
                handleFormSubmission(calendar,form, eventId);
                modalEvent.hide();
            });

            delete_btn.addEventListener('click', () => {
                fetch(routes.app_head_service_delete_planned_task + eventId)
                    .then(response => {
                        if(response.ok)
                        calendar.getEventById(eventId).remove();
                    })
                    .catch(error => {
                        console.error('Erreur :', error);
                    });
            });
        })
        .catch(error => {
            console.error('Erreur :', error);
        });
}

function handleFormSubmission(calendar,form, eventId = null, planningId = null) {
    var formData = new FormData(form);
    var url = (eventId) ? routes.app_head_service_edit_planned_task + eventId : routes.app_head_service_planning_add_planned_task + planningId;


    fetch(url, {
        method: 'POST',
        body: formData
    })
        .then(response => {
            if (!response.ok) {
                throw new Error('Erreur lors de la soumission du formulaire');
            }

            return response.json();
        })
        .then(data => {
            const eventData = {
                id: data.id,
                title: data.title,
                startTime: data.startTime, 
                endTime: data.endTime,
                daysOfWeek:data.daysOfWeek, 
                color: data.color,
                extendedProps: {
                    eventDescription: data.extendedProps.eventDescription,
                    eventPlace: data.extendedProps.eventPlace
                }
            };
            console.log(eventData);
            calendar.addEvent(eventData);
        })
        .catch(error => {
            console.error('Erreur :', error);
        });
}

// function reloadCalendar(calendar) {
//     console.log(calendar);
//     initCalendarWeekly();
// }
