const routes = {
    app_head_service_planning_get_events :'/head/service/planning/get/events/',
    app_head_service_planning_add_planned_task : '/head/service/planning/planned/task/add/event/',
    app_head_service_edit_planned_task : '/head/service/planning/planned/task/edit/event/',
    app_head_service_edit_drop_planned_task: '/head/service/planning/planned/task/edit/drop/event/',
    app_head_service_delete_planned_task: '/head/service/planning/planned/task/delete/event/',
    app_head_service_planning_finalize: '/head/service/planning/finalize/',
    app_head_service_time_slot_get_events_ajax: '/head/service/time/slot/get/events/ajax/'

}
export default routes;