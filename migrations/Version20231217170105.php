<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231217170105 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE time_slot ADD planned_task_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE time_slot ADD CONSTRAINT FK_1B3294ACD9EE6B2 FOREIGN KEY (planned_task_id) REFERENCES planned_task (id)');
        $this->addSql('CREATE INDEX IDX_1B3294ACD9EE6B2 ON time_slot (planned_task_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE time_slot DROP FOREIGN KEY FK_1B3294ACD9EE6B2');
        $this->addSql('DROP INDEX IDX_1B3294ACD9EE6B2 ON time_slot');
        $this->addSql('ALTER TABLE time_slot DROP planned_task_id');
    }
}
