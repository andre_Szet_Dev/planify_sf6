<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231212101609 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE time_slot DROP FOREIGN KEY FK_1B3294A6BF700BD');
        $this->addSql('DROP INDEX IDX_1B3294A6BF700BD ON time_slot');
        $this->addSql('ALTER TABLE time_slot ADD status VARCHAR(255) DEFAULT NULL, DROP status_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE time_slot ADD status_id INT DEFAULT NULL, DROP status');
        $this->addSql('ALTER TABLE time_slot ADD CONSTRAINT FK_1B3294A6BF700BD FOREIGN KEY (status_id) REFERENCES status (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_1B3294A6BF700BD ON time_slot (status_id)');
    }
}
