<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231206164406 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE planned_task ADD establishment_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE planned_task ADD CONSTRAINT FK_E926B6B88565851 FOREIGN KEY (establishment_id) REFERENCES establishment (id)');
        $this->addSql('CREATE INDEX IDX_E926B6B88565851 ON planned_task (establishment_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE planned_task DROP FOREIGN KEY FK_E926B6B88565851');
        $this->addSql('DROP INDEX IDX_E926B6B88565851 ON planned_task');
        $this->addSql('ALTER TABLE planned_task DROP establishment_id');
    }
}
